const path = require("path");
const https = require("https");
const fs = require("fs");
const { promisify } = require("util");

const express = require("express");
const cors = require("cors");
const { MerrorMiddleware, Merror } = require("express-merror");
const morgan = require("morgan");
const rfs = require("rotating-file-stream");

const { listenPort, twoWaySSL, corsOptions } = require("./config");
const { pmControllerInfo } = require("./controller/pm");
const { authMiddleware } = require("./middleware/auth");

const readFile = promisify(fs.readFile);

function setupApp() {
  const app = express();

  // Configure CORS.
  app.use(cors(corsOptions));

  const morganOptions = {};
  if (process.env.NODE_ENV == "test") {
    morganOptions.stream = rfs("access.log", {
      interval: "1d", // Rotate daily.
      path: path.join(path.dirname(__dirname), "log")
    });
  }
  app.use(morgan("combined", morganOptions));

  app.use(express.json());

  app.use(authMiddleware);

  // Register Routing Module.
  const router = express.Router();
  app.use("/", router);

  // Register controllers.
  [].concat(pmControllerInfo).forEach(info => {
    router.post(info.path, info.validations, info.handler);
  });

  // Handle 404, not found.
  app.use((req, res, next) => {
    return next(new Merror(404, "Unable to find requested resource."));
  });

  app.use(MerrorMiddleware());

  return app;
}

async function startApp(app) {
  serverPK = await readFile(twoWaySSL.server.pkPath, { encoding: "utf8" });
  serverCert = await readFile(twoWaySSL.server.certPath, { encoding: "utf8" });

  https
    .createServer({ key: serverPK, cert: serverCert }, app)
    .listen(listenPort);
}

module.exports.setupApp = setupApp;
module.exports.startApp = startApp;

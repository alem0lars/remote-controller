const fs = require("fs");
const { promisify } = require("util");

const vaultInitializer = require("node-vault");
const { Merror } = require("express-merror");

const { secret: secretConfig, jwt: jwtConfig, pepper } = require("../config");
const { decodeB64Url } = require("../util/b64");
const { jwtVerify, jwtGenerateAuthToken } = require("../util/jwt");
const { generateBearer, parseBearer } = require("../util/bearer-token");
const { createError } = require("../util/error");

const readFile = promisify(fs.readFile);

async function authMiddleware(req, res, next) {
  let vault;
  if (secretConfig.method === "vault") {
    let token = await readFile(secretConfig.tokenPath, { encoding: "utf8" });
    token = token.trim();
    vault = vaultInitializer({
      endpoint: secretConfig.endpoint,
      apiVersion: secretConfig.apiVersion,
      token
    });
  }

  try {
    // 1: Parse authorization token.
    let authToken = parseBearer(req.headers.authorization);
    if (!authToken) {
      return next(new Merror(403, "Missing Authorization header."));
    }

    // 2: Load secret from vault.
    let secret;
    if (secretConfig.method === "vault") {
      try {
        const vaultReply = await vault.read(secretConfig.path);
        secret = vaultReply.data[secretConfig.tokenKey];
      } catch (error) {
        return next(
          createError(500, "Failed to read Vault secret.", error.message)
        );
      }
    } else if (secretConfig.method === "dummy") {
      // Support tests.
      secret = secretConfig.value;
    } else {
      return next(
        new Merror(500, "Invalid secret method.", {
          method: secretConfig.method
        })
      );
    }
    const secretWithPepper = `${secret}${pepper}`;

    // 3: Load JWT certificate and private key.
    let jwtClientCert;
    try {
      jwtClientCert = await readFile(jwtConfig.client.certPath, {
        encoding: "utf8"
      });
    } catch (error) {
      return next(
        createError(500, "Failed to read client JWT cert.", error.message)
      );
    }
    let jwtServerPK;
    try {
      jwtServerPK = await readFile(jwtConfig.server.pkPath, {
        encoding: "utf8"
      });
    } catch (error) {
      return next(
        createError(500, "Failed to read server JWT PK.", error.message)
      );
    }

    // 3: Compare input secret (JWT or plain) with the one read from vault.
    try {
      const decodedToken = await jwtVerify(authToken, jwtClientCert);
      if (decodedToken.secret !== secretWithPepper) {
        return next(
          createError(403, "Invalid secret in JWT token.", {
            secret: secretWithPepper,
            inputSecret: decodedToken.secret
          })
        );
      }
    } catch (error) {
      authToken = decodeB64Url(authToken);
      if (authToken !== secret) {
        return next(
          createError(403, "Invalid secret.", {
            secret,
            inputSecret: authToken
          })
        );
      }

      try {
        const jwtToken = await jwtGenerateAuthToken(
          secretWithPepper,
          jwtServerPK
        );
        authToken = generateBearer(jwtToken);
      } catch (error) {
        return next(createError(500, "Failed to generate JWT.", error.message));
      }
    }

    // 4: Return JWT token in Authorization header.
    res.set("Authorization", authToken);

    next();
  } catch (error) {
    return next(createError(500, "Unhandled error.", error.message));
  }
}

module.exports.authMiddleware = authMiddleware;

const { encodeB64Url, decodeB64Url } = require("./b64");

function parseBearer(authHeader, b64Decode) {
  if (!authHeader) {
    return null;
  }

  const parts = authHeader.split(" ");

  if (parts.length != 2) {
    return null;
  }

  const schema = parts[0];
  if (!/bearer[:]?/i.test(schema)) {
    return null;
  }

  return b64Decode ? decodeB64Url(parts[1]) : parts[1];
}

function generateBearer(token, b64Encode) {
  if (b64Encode) {
    token = encodeB64Url(token);
  }
  return `Bearer: ${token}`;
}

module.exports.parseBearer = parseBearer;
module.exports.generateBearer = generateBearer;

const { promisify } = require("util");

const jwt = require("jsonwebtoken");

const jwtSign = promisify(jwt.sign);
const jwtVerify = promisify(jwt.verify);

async function jwtGenerateAuthToken(secret, signPK) {
  return await jwtSign({ secret }, signPK, { algorithm: "RS256" });
}

module.exports.jwtVerify = jwtVerify;
module.exports.jwtGenerateAuthToken = jwtGenerateAuthToken;

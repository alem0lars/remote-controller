const { Merror } = require("express-merror");

function createError(statusCode, message, testData) {
  return new Merror(
    statusCode,
    message,
    process.env.NODE_ENV === "production" ? {} : testData
  );
}

module.exports.createError = createError;

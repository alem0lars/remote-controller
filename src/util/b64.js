function encodeB64Url(input, encoding) {
  if (!encoding) {
    encoding = "utf8";
  }

  if (Buffer.isBuffer(input)) {
    return fromBase64(input.toString("base64"));
  }

  return fromBase64(Buffer.from(input, encoding).toString("base64"));
}

function decodeB64Url(base64url, encoding) {
  if (!encoding) {
    encoding = "utf8";
  }

  return Buffer.from(toBase64(base64url), "base64").toString(encoding);
}

function toBase64(base64url) {
  // We this to be a string so we can do .replace on it. If it's already a
  // string, this is a noop.
  base64url = base64url.toString();

  return padString(base64url)
    .replace(/\-/g, "+")
    .replace(/_/g, "/");
}

function fromBase64(base64) {
  return base64
    .replace(/=/g, "")
    .replace(/\+/g, "-")
    .replace(/\//g, "_");
}

function padString(input) {
  let segmentLength = 4;
  let stringLength = input.length;
  let diff = stringLength % segmentLength;

  if (!diff) {
    return input;
  }

  let position = stringLength;
  let padLength = segmentLength - diff;
  let paddedStringLength = stringLength + padLength;
  let buffer = Buffer.alloc(paddedStringLength);

  buffer.write(input);

  while (padLength--) {
    buffer.write("=", position++);
  }

  return buffer.toString();
}

module.exports.encodeB64Url = encodeB64Url;
module.exports.decodeB64Url = decodeB64Url;
